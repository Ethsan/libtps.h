#include <tps.h>
#include <stdio.h>

int main(void) 
{
    tps_stack stack = tps_stack_create();
    for(int i = 0; i < 150; i++)
    {
        tps_stack_push(stack, i);
    }
    printf("%d\n", tps_stack_size(stack));
}