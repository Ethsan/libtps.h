VERSION := 1.2.0
MAJOR_VERSION := $(shell echo $(VERSION) | head -c 1)

OS := $(shell uname)


# installation directory (/usr/local by default)

PREFIX ?= /usr/local
INSTALL_PATH = $(DESTDIR)$(PREFIX)

NO_UI ?= 0

ifneq ($(NO_UI),0)
IN_BROWSER_FLAG=-DNO_UI
else
IN_BROWSER_FLAG=
endif

INCLUDE_PATH ?= /usr/local/include

ifneq (,$(filter Linux Darwin,$(OS)))
	LIB_PATH?=/usr/local/lib
else
	LIB_PATH?=C:\dev\sdl2\lib
endif

LD_LIBRARY_PATH ?= /usr/local/lib


INSTALL_CMD = install #install -D

SOURCE := $(wildcard *.c)
OBJECT := $(SOURCE:.c=.o)

CC ?= clang

# Linux
ifeq ($(OS),Linux)
	LIB_BASE := libtps.so
	LIB_MAJOR := libtps.so.$(MAJOR_VERSION)
	LIB_VERSION := libtps.so.$(VERSION)
	LINKER_FLAGS := -Wl,-soname,$(LIB_VERSION)
# Mac
else ifeq ($(OS),Darwin)
	LIB_BASE := libtps.dylib
	LIB_MAJOR := libtps-$(MAJOR_VERSION).dylib
	LIB_VERSION := libtps-$(VERSION).dylib
	LINKER_FLAGS := -Wl,-install_name,$(LIB_VERSION)
endif

CFLAGS ?= -std=c11 -Wall -Wextra -Wno-unused-const-variable



SRCDIR   = src
OBJDIR   = obj

SOURCES  := $(wildcard $(SRCDIR)/*.c)
OBJECTS  := $(SOURCES:$(SRCDIR)/%.c=$(OBJDIR)/%.o)

all: clean build


$(OBJECTS): $(OBJDIR)/%.o : $(SRCDIR)/%.c
	mkdir -p $(OBJDIR)
	$(CC) -c -fPIC $(CFLAGS) -g -o $@ -I$(INCLUDE_PATH) $(IN_BROWSER_FLAG) $<

build: $(OBJECTS)
	$(CC) -shared $(LINKER_FLAGS) -g -o $(LIB_VERSION) -L$(LD_LIBRARY_PATH) -L$(LIB_PATH) -lSDL2 -lSDL2_ttf $(IN_BROWSER_FLAG) $^
	rm -f tps.o
	ln -s $(LIB_VERSION) $(LIB_MAJOR)
	ln -s $(LIB_MAJOR) $(LIB_BASE)
	mkdir -p build/include
	$(INSTALL_CMD) -m 644 src/*.h build/include
	mkdir -p build/lib build/src/libtps
	mv $(LIB_VERSION) build/lib
	mv $(LIB_MAJOR) build/lib
	mv $(LIB_BASE) build/lib




.PHONY: install
install:
	mkdir -p $(INSTALL_PATH)
	cp -r build/* $(INSTALL_PATH)
	mkdir -p $(HOME)/.config
	chmod 755 $(HOME)/.config
	cp VERSION $(HOME)/.config/libtps_version
	chmod a+wr $(HOME)/.config/libtps_version

.PHONY: clean
clean:
	rm -rf build
	rm -f obj/*.o


# used by .travis.yml
.PHONY: version
version:
	@echo $(VERSION)

doc: src/tps.h src/kirby.h
	python3 docgen.py src/kirby.h docs/kirby.h.adoc
	python3 docgen.py src/tps_gui.h docs/tps_gui.h.adoc
	python3 docgen.py src/tps_stack.h docs/tps_stack.h.adoc
	python3 docgen.py src/tps_utils.h docs/tps_utils.h.adoc
	asciidoctor -b html5 docs/kirby.h.adoc
	asciidoctor -b html5 docs/tps_stack.h.adoc
	asciidoctor -b html5 docs/tps_gui.h.adoc
	asciidoctor -b html5 docs/tps_utils.h.adoc
	asciidoctor -b html5 docs/tps-cli.adoc
	asciidoctor -b html5 docs/index.adoc
