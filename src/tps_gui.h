
//#define TPS_IN_BROWSER

#ifndef LIBTPS_GUI_H_INCLUDE
#define LIBTPS_GUI_H_INCLUDE

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include "tps_noise.h"

#define true 1
#define false 0


/**

= Librairie TPS - Fonctions graphiques
Quentin Bramas <bramas@unistra.fr>

Cette librairie contient plusieurs fonctions permettant d'afficher des éléments simples dans une fenêtre graphique.


Un programme graphique simple consiste à créer une fenêtre <<tps_createWindow>>, à boucler tant que l'utilisateur ne quitte pas <<tps_isRunning>>, puis à fermer la fenêtre <<tps_closeWindow>>.

Pour dessiner, il faut appeler des fonctions de dessin comme <<tps_drawRect>> ou <<tps_fillTriangle>> par exemple, puis à appeler la fonction <<tps_render>> pour que le dessin s'affiche sur la fenêtre.

La fonction <<tps_background>> permet d'effacer le dessin avec une couleur donnée.

Exemple complet:

```c
#include <tps.h>

int main(void) {
  tps_createWindow("Titre", 800, 600);
  while(tps_isRunning()) {
    tps_background(255,255,255);
    tps_setColor(0,0,0);
    tps_fillEllipse(200, 200, 50, 150);
    tps_render();
  }
  tps_closeWindow();
  return 0;
}
```

La compilation doit inclure la librairie tps et les librairies sdl:
```
gcc -o prog prog.c -ltps -lSDL2 -lSDL2_ttf
```

*/


/**
=== Description
Crée une fenêtre ayant un titre et une taille donnée.
La fenêtre reste ouverte jusqu'à la fin de l'execution du programme. Il faut donc empecher la fin du programme en utilisant une boule `while` tant que la fonction <<tps_isRunning>> retourne `vrai`.

De plus, il est préférable d'appeler la fonction <<tps_closeWindow>> à la fin de l'exécution du programme.

Exemple:
```c
#include <tps.h>

int main(void) {
  tps_createWindow("Titre", 800, 600);
  while(tps_isRunning()) {
    // Dessiner ici
  }
  tps_closeWindow();
  return 0;
}
```

=== Paramètres
* `title`: le titre de la fenêtre
* `width`: la largeur de la fenêtre (en pixels)
* `height`: la hauteur de la fenêtre (en pixels)

=== Retour
Cette fonction retourne 1 si la création s'est bien passée et retourn 0 en cas d'erreur.
*/
int tps_createWindow(char* title, int width, int height);

/**
=== Description
Ferme la fenêtre
*/
void tps_closeWindow(void);
/**
=== Description
Effectue le rendue de la fenêtre. Cette fonction doit être appelée pour que les dessins effectuée s'affiche à l'ecran.
*/
void tps_render(void);
/**
=== Description
Permet de savoir si l'utilisateur a quitté l'application ou si elle toujours en train de s'exécutée.
*/
int tps_isRunning(void);

/**
=== Description
Choisir la couleur du pinceau. Toutes les figures dessinées par la suite seront de cette couleur, jusqu'au prochain appel de cette fonction.
Les arguments `r`, `g` et `b` correspondent à la quantité de rouge, de vert et de bleu. *Ce sont des nombres compris entre 0 et 255*.
*/
void tps_setColor(int r, int g, int b);
/**
=== Description
Choisir la couleur, possédant de la transparence, du pinceau. Toutes les figures dessinées par la suite seront de cette couleur, jusqu'au prochain appel de cette fonction.
Voir <<tps_setColor>>
*/
void tps_setColorAlpha(int r, int g, int b, int a);

/**
=== Description
Efface la fenêtre et la remplie avec la couleur donnée.
*/
void tps_background(int r, int g, int b);


/**
=== Description
Dessine un segment entre les points `(x0, y0)` et `(x1, y1)`. La couleur du pixel dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_drawLine(int x0, int y0, int x1, int y1);

/**
=== Description
Dessine un pixel à la position `(x, y)` donnée. La couleur du pixel dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_drawPoint(int x, int y);

/**
=== Description
Dessine un triangle.  La couleur du rectangle dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_drawTriangle(int x0, int y0, int x1, int y1, int x2, int y2);
/**
=== Description
Dessine l'intérieur un triangle.  La couleur du rectangle dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_fillTriangle(int x0, int y0, int x1, int y1, int x2, int y2);

/**
=== Description
Dessine un rectangle à la position `(x, y)` donnée, de largeur `w` et de longueur `h`.  La couleur du rectangle dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_drawRect(int x, int y, int w, int h);

/**
=== Description
Colorie l'intérieur d'un rectangle à la position `(x, y)` donnée, de largeur `w` et de longueur `h`.  La couleur du rectangle dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_fillRect(int x, int y, int w, int h);

/**
=== Description
Dessine une ellipse à la position `(x, y)` donnée, de horizontal `rx` et de rayon vertival `ry`.  La couleur du ellipse dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_drawEllipse(int xc, int yc, int rx, int ry);

/**
=== Description
Colorie l'intérieur d'une ellipse à la position `(x, y)` donnée, de horizontal `rx` et de rayon vertival `ry`.  La couleur du ellipse dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_fillEllipse(int xc, int yc, int rx, int ry);

/**
=== Description
Dessine le texte `t` à la position `(x, y)` donnée, avec une taille de police `fontSize`.  La couleur du texte dépend du dernier appel à la fonction <<tps_setColor>> ou <<tps_setColorAlpha>>.
*/
void tps_drawText(int x, int y, char* t, int fontSize);


double tps_getFPS(void);

/**
=== Description
Ecrit le nombre de frame par seconde, dans un coin de la fenêtre.
*/
void tps_drawFPS(void);

/**
=== Description
Retourne la touche qui viens d'être pressée (ou 0 si aucune touche n'a été pressée). Utiliser les constantes
issuent de la librairie SDL pour tester quelle touche
vient d'être pressée.
*/
int tps_getKeyPressed(void);


void tps_onKeyDown(void (*handle)(int));
void tps_onKeyUp(void (*handle)(int));

/**
=== Description
Permet de récupérer la position de la souris

=== Exemple

```C
int x, y;
tps_getMousePosition(&x, &y);
printf("La souris est à la position %i, %i\n", x, y);
```
*/
void tps_getMousePosition(int *x, int *y);

/**
=== Description
Permet de récupérer l'état des boutons de la souris. La valeur est issue la même que celle utilisée par la librairie SDL

=== Exemple

```C
int buttons = tps_getMouseButtons();
if(buttons & SDL_BUTTON(SDL_BUTTON_LEFT)){
  printf("Le boutton gauche de la souris est pressé\n");
}

```
*/
int tps_getMouseButtons(void);

/**
=== Description
Permet de savoir si un click de souris viens d'être effectué.

=== Exemple

```C
if(tps_mouseIsClicked()){
  printf("Click\n");
}

```
*/
int tps_mouseIsClicked(void);

/**
=== Description
Retourne la durée en millisecondes qui s'est écoulée
depuis le dernier rendu
*/
Uint32 tps_getDeltaTime(void);

/**
=== Description
Permet de mettre en pause l'exécution du programme
pendant un certains temps (en milliseconds).

La fenêtre freezée pendant la pause.
*/
void tps_wait(unsigned int milliseconds);


SDL_Renderer* tps_getRenderer(void);


typedef struct tps_texture {
  SDL_Texture *sdl_texture;
  Uint32 * pixels;
  Uint32 width;
  Uint32 height;
} tps_texture_t;


tps_texture_t * tps_createTexture(long width, long height);

void tps_drawTexture(tps_texture_t *texture);

void tps_freeTexture(tps_texture_t *texture);

#endif
