/**
= Le monde de Kirby
Quentin Bramas <bramas@unistra.fr>

Librairie permettant de faire évoluer Kirby dans un monde imaginaire et de lui faire exécuter des taches.

Inclure l'entête <kirby.h> :
[source,C]
----
#include <kirby.h>
----

*/

#ifndef KIRBY_H_INCLUDE
#define KIRBY_H_INCLUDE

#include "tps.h"
#include "kirby-bag.h"
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>




void createWorldFromFile(char * mapFilename);

/**
=== Description
Crée le monde de Kirby. La description du monde est lue depuis l'entrée standart

=== Exemple
[source,C]
----
#include <kirby.h>

int main(void) {
  createWorld();
  cleanWorld();
  return 0;
}
----
Compiler avec les options suivantes:
[source]
----
gcc -o kirby kirby.c -ltps -lSDL2 -lSDL2_ttf
----

Exécuter en fournissant une carte dans l'entrée standart
[source]
----
./kirby < chemin/vers/map.kirby-map
----
*/
void createWorld();

void createWorldFromStdin();

/**
=== Description
Néttoie de monde de Kriby avant que le programme quitte. Cette fonction doit être appeler avant que le programme ne quitte. Après avoir appelé cette fonction, le monde de Kirby n'existe plus.
*/
void cleanWorld();

/**
=== Description
Définie la vitesse des actions de Kirby, en millisecondes

=== Paramètres
`t` est la vitesse en milliseconde
*/
void setSpeed(Uint32 t);

/**
=== Description
Retourne vrai si Kirby peut avancer
*/
int frontIsClear();
/**
=== Description
Retourne vrai si Kirby ne peut pas avancer
*/
int frontIsBlocked();
/**
=== Description
Retourne vrai s'il n'y a pas de mur sur la gauche de Kirby
*/
int leftIsClear();
/**
=== Description
Retourne vrai s'il n'y a pas de mur sur la droite de Kirby
*/
int rightIsClear();
/**
=== Description
Fait avancer Kirby d'une case devant lui.
*/
void move();
/**
=== Description
Modifie l'orientation de Kirby en le faisant tourner vers la gauche.
Kirby reste à la même position durant cette opération, uniquement son orientation est modifiée. S'il regardait vers le haut avant l'appel à cette fonction, il regardera vers la gauche juste arpès l'appel à cette fonction.
*/
void turnLeft();
/**
=== Description
Indique si le sac de Kirby est vide.
*/
int bagIsEmpty();
/**
=== Description
Retourne vrai si Kirby se situe sur un fruit (enfin, sur une case qui contient un fruit. Si c'est le cas, Kriby peut ramasser le fruit, donc il n'est pas vraiment _dessus_)
*/
int isOnFruit();
/**
=== Description
Rammasse le fruit qui se trouve sur la même case que Kirby

Kirby doit se trouver sur un fruit, sinon il y aura une erreur.

voir aussi <<isOnFruit>>
*/
kb_fruit getFruit();
/**
=== Description
Dépose un fruit sur la même case que Kirby

Kirby doit avoir au moins un fruit dans son sac, sinon il y aura une erreur
*/
void putFruit();
/**
=== Description
retourne le premier fruit se trouvant dans le sac (le dernier qui a été mis dans le sac)

Kirby doit avoir au moins un fruit dans son sac, sinon il y aura une erreur
*/
kb_fruit topFruit();

#define BANANA 'b'
#define APPLE 'a'
#define CHERRY 'c'
#define DATE 'd'
#define EGGFRUIT 'd'
#define FIG 'f'
#define GRAPEFRUIT 'g'


/**
=== Description
Indique si le fruit donné est une pomme
*/
int isApple(kb_fruit f);
/**
=== Description
Indique si le fruit donné est une banane
*/
int isBanana(kb_fruit f);


void KB_stop();
int KB_update();

#ifdef TPS_IN_BROWSER
int _started = 1;
#else
int _started = 0;
#endif



#ifdef NO_UI

#define createWorldFromFile NO_UI_createWorldFromFile

#define KB_stop KB_NO_UI_stop
#define KB_update KB_NO_UI_update


#define move NO_UI_move
#define turnLeft NO_UI_turnLeft
#define getFruit NO_UI_getFruit
#define putFruit NO_UI_putFruit


#endif




#define SECOND(first, second, ...) second
#define SECOND_(...) SECOND(__VA_ARGS__)
#define TRIGGER(...) , NULL
#define NULL_IF_EMPTY(value) SECOND_(TRIGGER value (), value)

#define createWorld(value) \
  createWorldFromFile(NULL_IF_EMPTY(value))


#endif
