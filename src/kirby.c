
#include "kirby.h"
#include "kirby-bag.h"
#include <ctype.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>


#include "tps_banana.c"
#include "tps_apple.c"


int _KB_isStopped = 0;

typedef struct _KB_info_t_ {
  int x;
  int y;
  int directionX;
  int directionY;
  Uint32 speed;
  kb_bag *bag;
} _KB_info_t;


char ** _KB_map = 0;
kb_bag *** _KB_fruitMap = 0;
int _KB_mapWidth;
int _KB_mapHeight;
char * _KB_error = 0;

#define MAP_WALL 'W'
#define MAP_FREESPACE '.'


_KB_info_t _KB_info = {
  .x=0,
  .y=0,
  .directionX=1,
  .directionY=0,
  .speed=500,
  .bag = 0
};

int _KB_cellSize = 50;



void _error(char * msg) {
  _KB_error = msg;
  _KB_isStopped = 1;
  cleanWorld();
  exit(1);
}

void _KB_loadMap(char * filename) {
  FILE *input_file = filename ? fopen(filename, "r") : stdin;
  if(!input_file) {
    return;
  }

  int fruitInBag = 0;
  // Load the bag
  fscanf(input_file, "%d", &fruitInBag);
  printf("fruitInBag : %d\n", fruitInBag);
  if(fruitInBag == -1) {
    kb_fruit f;
    fscanf(input_file, " %c", &f);
    printf("F : %c\n", f);
    _KB_info.bag = kb_bag_createInfinite(f);
    fscanf(input_file, " %d", &fruitInBag);
  } else {
    _KB_info.bag = kb_bag_create();
  }

  for(int i = 0; i < fruitInBag; ++i)
  {
    kb_fruit f;
    fscanf(input_file, " %c", &f);
    kb_bag_putFruit(_KB_info.bag, f);
  }

  fscanf(input_file, "%d %d", &_KB_mapWidth, &_KB_mapHeight);
  printf( "loaded map of size %d %d\n", _KB_mapWidth, _KB_mapWidth);
  _KB_map = (char**)malloc(_KB_mapWidth*sizeof(char*));
  _KB_fruitMap = (kb_bag***)malloc(_KB_mapWidth*sizeof(kb_bag**));

  for(int x = 0; x < _KB_mapWidth; ++x) {
    _KB_map[x] = (char*)malloc(_KB_mapHeight*sizeof(char));
    _KB_fruitMap[x] = (kb_bag**)calloc(_KB_mapHeight,sizeof(kb_bag*));
  }

  for(int y = 0; y < _KB_mapHeight; ++y)
  {
    fscanf(input_file, "\n");
    for(int x = 0; x < _KB_mapWidth; ++x) {
      _KB_map[x][y] = fgetc(input_file);
      printf( "%c", _KB_map[x][y]);
    }
    printf( "\n");
  }
  int fruitCount = 0;
  fscanf(input_file, "%d", &fruitCount);
  for(int i = 0; i < fruitCount; ++i) {
    int x,y;
    kb_fruit f;
    fscanf(input_file, "%d %d %c\n", &x, &y, &f);
    printf("-> %d %d %c\n", x, y, f);
    if(!_KB_fruitMap[x][y])
      _KB_fruitMap[x][y] = kb_bag_create();
    kb_bag_putFruit(_KB_fruitMap[x][y], f);
  }
  fscanf(input_file, "%d %d", &_KB_info.x, &_KB_info.y);
}

#define NARGS(...) NARGS_(__VA_ARGS__, 5, 4, 3, 2, 1, 0)
#define NARGS_(_5, _4, _3, _2, _1, N, ...) N


void KB_drawKirby() {
  tps_setColor(255,0,0);
  int cx = _KB_info.x*_KB_cellSize + _KB_cellSize/2;
  int cy = _KB_info.y*_KB_cellSize + _KB_cellSize/2;
  int w1 = 9, h1 = 15, w2 = 11, h2 = 3;
  if(_KB_info.directionY == 0)
    if(_KB_info.directionX == 1) {
      tps_fillEllipse(cx - 6, cy, w1, h1);
      tps_fillEllipse(cx - 1, cy - 8, w2, h2);
      tps_fillEllipse(cx - 1, cy + 8, w2, h2);
    } else {
      tps_fillEllipse(cx + 6, cy, w1, h1);
      tps_fillEllipse(cx + 1, cy - 8, w2, h2);
      tps_fillEllipse(cx + 1, cy + 8, w2, h2);
    }
  else
    if(_KB_info.directionY == -1) {
      tps_fillEllipse(cx, cy + 6, h1, w1);
      tps_fillEllipse(cx - 8, cy + 1, h2, w2);
      tps_fillEllipse(cx + 8, cy + 1, h2, w2);
    } else {
      tps_fillEllipse(cx, cy - 6, h1, w1);
      tps_fillEllipse(cx - 8, cy - 1, h2, w2);
      tps_fillEllipse(cx + 8, cy - 1, h2, w2);
    }
}

void drawSprite(int x, int y, const uint32_t *sprite) {
  int index = 0;
  for(int j = 0; j < 10; ++j){
    for(int i = 0; i < 10; ++i){
      if(sprite[index] & 0xff000000) {
        tps_setColor((sprite[index]) & 0xff, (sprite[index]>>8) & 0xff,(sprite[index]>>16) & 0xff);
        tps_drawPoint(x+i, y+j);
      }
      index++;
    }
  }
}


void KB_drawCell(int x, int y) {
  if(_KB_map[x][y] == MAP_WALL){
    tps_setColor(0,0,0);
    tps_fillRect(x*_KB_cellSize, y*_KB_cellSize, _KB_cellSize, _KB_cellSize);
  }
  if(_KB_fruitMap[x][y] && !kb_bag_isEmpty(_KB_fruitMap[x][y]))
  {
    char t[10];
    sprintf(t, "%d", kb_bag_size(_KB_fruitMap[x][y]));
    tps_setColor(255,0,255);
    tps_drawText(20 + x*_KB_cellSize, y*_KB_cellSize, t, 20);
    char f = kb_bag_topFruit(_KB_fruitMap[x][y]);
    if(f == 'a') {
      drawSprite(3+x*_KB_cellSize, 3+y*_KB_cellSize, apple_data);
    } else if (f == 'b'){
      drawSprite(3+x*_KB_cellSize, 3+y*_KB_cellSize, banana_data);
    }

  }
}



void KB_drawMap() {
  tps_setColor(180,180,180);
  tps_fillRect(0, 0, _KB_mapWidth * _KB_cellSize, 1);
  tps_fillRect(0, 0, 1, _KB_mapHeight * _KB_cellSize);
  tps_fillRect(0, _KB_mapHeight * _KB_cellSize - 1, _KB_mapWidth * _KB_cellSize, 1);
  tps_fillRect(_KB_mapWidth * _KB_cellSize - 1, 0, 1, _KB_mapHeight * _KB_cellSize);

  for(int i = 0; i < _KB_mapWidth; ++i)
    tps_fillRect(i* _KB_cellSize, 0, 1, _KB_mapHeight * _KB_cellSize);
  for(int i = 0; i < _KB_mapHeight; ++i)
    tps_fillRect(0,i* _KB_cellSize, _KB_mapWidth * _KB_cellSize, 1);

  for(int y = 0; y < _KB_mapHeight; ++y)
    for(int x = 0; x < _KB_mapWidth; ++x)
      KB_drawCell(x,y);

}
void KB_drawCommands() {
  if(_started) return;

  int x, y;
  SDL_PumpEvents();
  tps_getMousePosition(&x, &y);
  int buttons = tps_getMouseButtons();
  int top = _KB_mapHeight * _KB_cellSize + 10;
  int bottom = top + 30;
  int left = 10;
  int right = 50;
  tps_setColor(255, 0, 0);
  if(y > top && y < bottom && x < right && x > left) {
    tps_setColor(0, 255, 0);
    if(buttons & SDL_BUTTON(SDL_BUTTON_LEFT)){
      _started = 1;
    }
  }
  tps_drawText(left, top, "Start", 16);
}


int KB_update() {
  if(_KB_isStopped) return 0;

  Uint32 lastTick = SDL_GetTicks();
  while(tps_isRunning()) {
    if((Uint32)(SDL_GetTicks() - lastTick) >= _KB_info.speed) {
      return 1;
    }
    tps_setColorAlpha(255, 255, 255, 255);
    tps_background(255, 255, 255);

    KB_drawMap();
    KB_drawKirby();
    KB_drawCommands();
    tps_render();
    //SDL_Delay(10);
  }
  _KB_isStopped = 1;
  return 0;
}


void KB_stop() {
  _KB_isStopped = 1;
  tps_closeWindow();
}


void createWorldFromFile(char * mapFilename) {
  _KB_loadMap(mapFilename);

  tps_createWindow("Kirby!!",_KB_mapWidth * _KB_cellSize,_KB_mapHeight * _KB_cellSize+40);

  if(!_KB_map) {
    KB_stop();
  }
  KB_update();
  while(tps_isRunning()) {
    if(_started || !KB_update()){
      return;
    }
  }
  _KB_isStopped = 1;
}


void createWorldFromStdin() {
  createWorldFromFile(NULL);
}


void applyOptionsFromArgv(int argc, char* argv[]) {

    char* option = "";
    for(int i = 0; i < argc; i++) {
      if(strcmp(option,"-s") == 0){
          setSpeed(strtoul(argv[i], NULL, 10));
      }
      option = argv[i];
    }
}

void setSpeed(Uint32 t) {
  _KB_info.speed = t;
}


int KB_NO_UI_update() {
  return 1;
}

int KB_continue() {
  return !_KB_isStopped && KB_update();
}


void NO_UI_createWorldFromFile(char * mapFilename) {
  _KB_loadMap(mapFilename);
  if(!_KB_map) {
    KB_stop();
  }
}

void KB_NO_UI_stop() {
  _KB_isStopped = 1;
}


void NO_UI_move() {
  if(!frontIsClear()) {
    _error("move() impossible: Impossible d'avancer!!");
    return;
  }
  _KB_info.x += _KB_info.directionX;
  _KB_info.y += _KB_info.directionY;
}

void NO_UI_turnLeft() {
  int t = _KB_info.directionX;
  _KB_info.directionX = _KB_info.directionY;
  _KB_info.directionY = -t;
}


kb_fruit NO_UI_getFruit() {
  kb_fruit ret = '\0';
  if(isOnFruit()) {
    kb_bag_putFruit(_KB_info.bag, kb_bag_getFruit(_KB_fruitMap[_KB_info.x][_KB_info.y]));
    ret = kb_bag_topFruit(_KB_info.bag);
  }
  else {
    _error("getFruit() impossible: Aucun fruit à rammasser");
  }
  return ret;
}

void NO_UI_putFruit() {
  if(bagIsEmpty()) {
    _error("putFruit() impossible: Aucun fruit à déposer");
  } else {
    if(!_KB_fruitMap[_KB_info.x][_KB_info.y])
      _KB_fruitMap[_KB_info.x][_KB_info.y] = kb_bag_create();
    kb_fruit f = kb_bag_getFruit(_KB_info.bag);
    kb_bag_putFruit(_KB_fruitMap[_KB_info.x][_KB_info.y], f);
  }
}


int KB_isPositionClear(int x, int y) {
  if(x < 0 ||
    x >= _KB_mapWidth ||
    y < 0 ||
    y >= _KB_mapHeight
  )
    return 0;

  if(_KB_map[x][y] == MAP_WALL)
    return 0;
  return 1;
}

int leftIsClear() {
  return KB_isPositionClear(
    _KB_info.x + _KB_info.directionY,
    _KB_info.y - _KB_info.directionX
  );
}
int rightIsClear() {
  return KB_isPositionClear(
    _KB_info.x - _KB_info.directionY,
    _KB_info.y + _KB_info.directionX
  );
}

int frontIsClear() {
  return KB_isPositionClear(_KB_info.x + _KB_info.directionX, _KB_info.y + _KB_info.directionY);
}

void move() {
  if(!frontIsClear()) {
    _error("move() impossible: Impossible d'avancer!!");
    return;
  }
  _KB_info.x += _KB_info.directionX;
  _KB_info.y += _KB_info.directionY;
  KB_update();
}

void turnLeft() {
  int t = _KB_info.directionX;
  _KB_info.directionX = _KB_info.directionY;
  _KB_info.directionY = -t;
  KB_update();
}

int bagIsEmpty() {
  return kb_bag_isEmpty(_KB_info.bag);
}
int isOnFruit() {
  if(!_KB_fruitMap[_KB_info.x][_KB_info.y])
    return 0;
  return !kb_bag_isEmpty(_KB_fruitMap[_KB_info.x][_KB_info.y]);
}

kb_fruit getFruit() {
  kb_fruit ret = '\0';
  if(isOnFruit()) {
    kb_bag_putFruit(_KB_info.bag, kb_bag_getFruit(_KB_fruitMap[_KB_info.x][_KB_info.y]));
    ret = kb_bag_topFruit(_KB_info.bag);
  }
  else {
    _error("getFruit() impossible: Aucun fruit à rammasser");
  }
  KB_update();
  return ret;
}
void putFruit() {
  if(bagIsEmpty()) {
    _error("putFruit() impossible: Aucun fruit à déposer");
  } else {
    if(!_KB_fruitMap[_KB_info.x][_KB_info.y])
      _KB_fruitMap[_KB_info.x][_KB_info.y] = kb_bag_create();
    kb_fruit f = kb_bag_getFruit(_KB_info.bag);
    kb_bag_putFruit(_KB_fruitMap[_KB_info.x][_KB_info.y], f);
  }
  KB_update();
}

kb_fruit topFruit() {
  if(bagIsEmpty()) {
    _error("topFruit() impossible: Aucun fruit dans le sac");
    return '\0';
  }
  return kb_bag_topFruit(_KB_info.bag);;
}

void cleanWorld() {

  if(kb_bag_size(_KB_info.bag) == -1)
  {
    printf("-1 %c ", kb_bag_infiniteFruit(_KB_info.bag));
  }
  printf("%i", kb_bag_nonInfiniteSize(_KB_info.bag));
  while(kb_bag_nonInfiniteSize(_KB_info.bag) > 0) {
    printf(" %c", kb_bag_getFruit(_KB_info.bag));
  }
  printf("\n");

  char s[10000] = "";
  int c = 0;
  for(int y = 0; y < _KB_mapHeight; ++y)
  {
    for(int x = 0; x < _KB_mapWidth; ++x) {
      if(_KB_fruitMap[x][y])
      {
        while(kb_bag_size(_KB_fruitMap[x][y]) > 0) {
          char pos[30];

          sprintf(pos, "%i %i %c\n", x, y, kb_bag_getFruit(_KB_fruitMap[x][y]));
          strcat(s, pos);
          c++;
        }
        kb_bag_free(_KB_fruitMap[x][y]);
      }
    }
  }
  printf("%i\n%s", c, s);
  printf("%i %i\n", _KB_info.x, _KB_info.y);


  if(_KB_error) {
    fprintf(stderr, "Error: %s\n", _KB_error);
  }

  for(int i = 0; i < _KB_mapWidth; ++i)
    free(_KB_map[i]);
  free(_KB_map);

  kb_bag_free(_KB_info.bag);

  for(int i = 0; i < _KB_mapWidth; ++i)
    if(_KB_fruitMap[i])
      free(_KB_fruitMap[i]);
  free(_KB_fruitMap);
}


int frontIsBlocked() { return !frontIsClear(); };


int isApple(kb_fruit f) { return f==APPLE; }

int isBanana(kb_fruit f) { return f==BANANA; }