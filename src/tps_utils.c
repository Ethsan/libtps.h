/**
= Librairie TPS - Fonctions utiles
Quentin Bramas <bramas@unistra.fr>

*/

#include "tps_utils.h"
#include <stdio.h>


void _print_memory_byte(void * address) {
  unsigned char c = *((signed char*)address);

  for(int i = 0; i < 8; ++i) {
    printf("%d", c & 0x80 ? 1 : 0);
    c = c << 1;
  }
}

void print_memory_byte(void * address) {
  _print_memory_byte(address);
  printf("\n");
}

void print_memory_bytes(void * address, int bytes) {
  for(int i = 0; i < bytes; ++i) {
    _print_memory_byte(address + i);
    printf(" ");
  }
  printf("\n");
}

