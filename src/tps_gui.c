
#include "tps.h"
#include "tps_stack.h"
#include "font-data.h"


#ifdef NO_UI

#define UNUSED(a) (void)a

int tps_createWindow(char* title, int width, int height) {
  UNUSED(title);
  UNUSED(width);
  UNUSED(height);
  return 1;
}

void tps_closeWindow() {
}
void tps_drawLine(int x, int y, int x2, int y2) {
  UNUSED(x);
  UNUSED(y);
  UNUSED(x2);
  UNUSED(y2);
}
void tps_background(int r, int g, int b) {
  UNUSED(r);
  UNUSED(g);
  UNUSED(b);
}
void tps_setColor(int r, int g, int b) {
  UNUSED(r);
  UNUSED(g);
  UNUSED(b);
}
void tps_setColorAlpha(int r, int g, int b, int a){
  UNUSED(r);
  UNUSED(g);
  UNUSED(b);
  UNUSED(a);
}
void tps_drawPoint(int x, int y){
  UNUSED(x);
  UNUSED(y);
}
void tps_drawPixels(unsigned int *pixels, int width, int height) {
  UNUSED(pixels);
  UNUSED(width);
  UNUSED(height);
}


void tps_drawRect(int x, int y, int w, int h){
  UNUSED(x);
  UNUSED(y);
  UNUSED(w);
  UNUSED(h);
}
void tps_fillRect(int x, int y, int w, int h){
  UNUSED(x);
  UNUSED(y);
  UNUSED(w);
  UNUSED(h);
}
void tps_drawEllipse(int xc, int yc, int rx, int ry){
  UNUSED(xc);
  UNUSED(yc);
  UNUSED(rx);
  UNUSED(ry);
}
void tps_fillEllipse(int xc, int yc, int rx, int ry){
  UNUSED(xc);
  UNUSED(yc);
  UNUSED(rx);
  UNUSED(ry);
}
void tps_drawText(int x, int y, char* t, int fontSize){
  UNUSED(x);
  UNUSED(y);
  UNUSED(t);
  UNUSED(fontSize);
}
void tps_drawTriangle(int x0, int y0, int x1, int y1, int x2, int y2){
  UNUSED(x0);
  UNUSED(y0);
  UNUSED(x1);
  UNUSED(y1);
  UNUSED(x2);
  UNUSED(y2);
}
void tps_fillTriangle(int x0, int y0, int x1, int y1, int x2, int y2){
  UNUSED(x0);
  UNUSED(y0);
  UNUSED(x1);
  UNUSED(y1);
  UNUSED(x2);
  UNUSED(y2);
}


double tps_getFPS(){
  return 1;
}
void tps_drawFPS(){

}


void tps_getMousePosition(int *x, int *y){
  UNUSED(x);
  UNUSED(y);
}
int tps_getMouseButtons(){
  return 0;
}

// Ne fait rien car pas d'équivalent dans le navigateur
void tps_render(){
}

int _nbFrames = 100;
int tps_isRunning() {
  _nbFrames = _nbFrames == 0 ? _nbFrames : _nbFrames - 1;
  return _nbFrames;
}


int tps_mouseIsClicked(void) {
  return 0;
}

#else

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>



SDL_Window* tps_WIN_window = 0;
SDL_Renderer* tps_WIN_renderer = 0;
Uint32 tps_deltaTime = 0;
Uint32 tps_lastRender = 0;
SDL_RWops * _fontRW = 0;

int _mouseX = -1;
int _mouseY = -1;
int _mouseButtons = 0;
int _keyPressed = 0;
int _mouseClickedButtons = 0;

void (*_onKeyDown)(int) = NULL;
void (*_onKeyUp)(int) = NULL;

void tps_onKeyDown(void (*handle)(int)) { _onKeyDown = handle; }
void tps_onKeyUp(void (*handle)(int)) { _onKeyUp = handle; }

int tps_createWindow(char* title, int width, int height) {
  if(SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    fprintf(stdout,"Échec de l'initialisation de la SDL (%s)\n",SDL_GetError());
    SDL_Quit();
    return 0;
  }

  tps_WIN_window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);
  if(tps_WIN_window == 0)
  {
    fprintf(stderr,"Erreur de création de la fenêtre: %s\n",SDL_GetError());
    SDL_Quit();
    return 0;
  }
  tps_WIN_renderer = SDL_CreateRenderer( tps_WIN_window, -1, SDL_RENDERER_ACCELERATED );

  if ( tps_WIN_renderer == 0 )
  {
    fprintf(stderr,"Erreur de création de lu renderer: %s\n",SDL_GetError());
    SDL_Quit();
    return 0;
  }

  if(TTF_Init() == -1)
  {
    fprintf(stderr, "Erreur d'initialisation de TTF_Init : %s\n", TTF_GetError());
    exit(EXIT_FAILURE);
    return 0;
  }
  _fontRW = SDL_RWFromConstMem(_font_data, sizeof(_font_data));

  SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" );
  SDL_SetRenderDrawBlendMode(tps_WIN_renderer, SDL_BLENDMODE_BLEND);

  SDL_RenderSetLogicalSize( tps_WIN_renderer, width, height );
  SDL_SetRenderDrawColor( tps_WIN_renderer, 255, 255, 255, 255);

  SDL_RenderClear( tps_WIN_renderer );
  SDL_RenderPresent( tps_WIN_renderer);
  return 1;
}

int tps_getKeyPressed(void) {
  return _keyPressed;
}

void tps_getMousePosition(int *x, int *y) {
  *x = _mouseX;
  *y = _mouseY;
}
int tps_getMouseButtons(void) {
  return _mouseButtons;
}

int tps_mouseIsClicked(void) {
  return _mouseClickedButtons;
}

void tps_closeWindow(void) {
  SDL_DestroyWindow(tps_WIN_window);
  tps_WIN_window = 0;
  TTF_Quit();
  SDL_Quit();
}

SDL_Renderer* tps_getRenderer(void) {
  return tps_WIN_renderer;
}

void tps_render(void) {
  Uint32 currentTick = SDL_GetTicks();
  tps_deltaTime = currentTick - tps_lastRender;
  tps_lastRender = currentTick;
  if(tps_deltaTime == 0) {
    tps_deltaTime = 1;
  }

  SDL_RenderPresent( tps_WIN_renderer);
}

Uint32 tps_getDeltaTime(void) {
  return tps_deltaTime;
}
int tps_isRunning(void) {
  if(!tps_WIN_window) return 0;
  
  _mouseClickedButtons = 0;
  SDL_Event event;
  while(SDL_PollEvent(&event) == 1) {
    switch (event.type)
    {
        case SDL_QUIT: {
          tps_closeWindow();
          return 0;
        }
        case SDL_KEYDOWN: {
            if(event.key.keysym.sym == SDLK_q && event.key.keysym.mod & (KMOD_CTRL | KMOD_ALT)) {
              tps_closeWindow();
              return 0;
            }
            _keyPressed = event.key.keysym.sym;
            if(_onKeyDown) _onKeyDown(event.key.keysym.sym);
            break;
        }
        case SDL_KEYUP:
            if(_onKeyUp) _onKeyUp(event.key.keysym.sym);
          _keyPressed = 0;
          break;
        case SDL_WINDOWEVENT:
          if(event.window.event == SDL_WINDOWEVENT_LEAVE) {
              _mouseY = _mouseX = -1;
          }
          break;
        case SDL_MOUSEMOTION:
          _mouseX = event.motion.x;
          _mouseY = event.motion.y;
          break;
        case SDL_MOUSEBUTTONDOWN:
          _mouseButtons |= SDL_BUTTON(event.button.button);
          if(event.button.clicks > 0)
            _mouseClickedButtons = SDL_BUTTON(event.button.button);
          break;
        case SDL_MOUSEBUTTONUP:
          _mouseButtons &= ~SDL_BUTTON(event.button.button);
          break;
    }
  }
  tps_wait(5);

  return 1;

}

void tps_setColor(int r, int g, int b) {
  SDL_SetRenderDrawColor( tps_WIN_renderer, r, g, b, 255);
}
void tps_setColorAlpha(int r, int g, int b, int a) {
  SDL_SetRenderDrawColor( tps_WIN_renderer, r, g, b, a);
}


void tps_background(int r, int g, int b) {
  Uint8 or;
  Uint8 og;
  Uint8 ob;
  Uint8 oa;
  SDL_GetRenderDrawColor(tps_WIN_renderer, &or,&og,&ob,&oa);
  SDL_SetRenderDrawColor( tps_WIN_renderer, r, g, b, 255);
  SDL_RenderClear( tps_WIN_renderer );
  SDL_SetRenderDrawColor( tps_WIN_renderer, or, og, ob, oa);
}
void tps_drawPoint(int x, int y) {
  SDL_RenderDrawPoint(tps_WIN_renderer, x,y);
}

void tps_drawLine(int x0, int y0, int x1, int y1) {
  SDL_RenderDrawLine(tps_WIN_renderer, x0, y0, x1, y1);
}

void tps_drawRect(int x, int y, int w, int h) {
  SDL_Rect rect;
  rect.x = x;
  rect.y = y;
  rect.w = w;
  rect.h = h;
  SDL_RenderDrawRect(tps_WIN_renderer, &rect);
}
void tps_fillRect(int x, int y, int w, int h) {
  SDL_Rect rect;
  rect.x = x;
  rect.y = y;
  rect.w = w;
  rect.h = h;
  SDL_RenderFillRect(tps_WIN_renderer, &rect);
}
void _fillBottomFlatTriangle(int x0, int y0, int x1, int y1, int x2, int y2)
{
  double invslope1 = (x1 - x0) / (double)(y1 - y0);
  double invslope2 = (x2 - x0) / (double)(y2 - y0);

  double curx1 = x0;
  double curx2 = x0;

  for (int scanlineY = y0; scanlineY <= y1; scanlineY++)
  {
    tps_drawLine((int)ceil(curx1), scanlineY, (int)floor(curx2), scanlineY);
    curx1 += invslope1;
    curx2 += invslope2;
  }
}
void _fillTopFlatTriangle(int x0, int y0, int x1, int y1, int x2, int y2)
{
  double invslope1 = (x2 - x0) / (double)(y2 - y0);
  double invslope2 = (x2 - x1) / (double)(y2 - y1);

  double curx1 = x2;
  double curx2 = x2;

  for (int scanlineY = y2; scanlineY > y0; scanlineY--)
  {
    tps_drawLine((int)ceil(curx1), scanlineY, (int)floor(curx2), scanlineY);
    curx1 -= invslope1;
    curx2 -= invslope2;
  }
}
void _swap(int *a, int *b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
}
void tps_drawTriangle(int x0, int y0, int x1, int y1, int x2, int y2)
{
  tps_drawLine(x0, y0, x1, y1);
  tps_drawLine(x0, y0, x2, y2);
  tps_drawLine(x1, y1, x2, y2);
}
void tps_fillTriangle(int x0, int y0, int x1, int y1, int x2, int y2)
{
   /* at first sort the three vertices by y-coordinate ascending so v1 is the topmost vertice */
  tps_drawTriangle(x0, y0, x1, y1, x2, y2);
  if(y0 > y1) {
    _swap(&x0, &x1);
    _swap(&y0, &y1);
  }
  if(y1 > y2) {
    _swap(&x1, &x2);
    _swap(&y1, &y2);
  }
  if(y0 > y1) {
    _swap(&x0, &x1);
    _swap(&y0, &y1);
  }

  /* here we know that v1.y <= v2.y <= v3.y */
  /* check for trivial case of bottom-flat triangle */
  if (y1 == y2)
  {
    _fillBottomFlatTriangle(x0, y0, x1, y1, x2, y2);
  }
  /* check for trivial case of top-flat triangle */
  else if (y0 == y1)
  {
    _fillTopFlatTriangle(x0, y0, x1, y1, x2, y2);
  }
  else
  {
    /* general case - split the triangle in a topflat and bottom-flat one */
    int x3 = (int)(x0 + ((double)(y1 - y0) / (double)(y2 - y0)) * (x2 - x0));
    int y3 = y1;
    _fillBottomFlatTriangle(x0, y0, x1, y1, x3, y3);
    _fillTopFlatTriangle(x1, y1, x3, y3, x2, y2);
  }
}

void tps_drawEllipse(int xc, int yc, int rx, int ry)
{
  int x, y, p;
  x = 0;
  y = ry;
  p = (ry * ry) - (rx * rx * ry) + ((rx * rx) / 4);
  while ((2 * x * ry * ry) < (2 * y * rx * rx)) {
    SDL_RenderDrawPoint(tps_WIN_renderer, xc + x, yc - y);
    SDL_RenderDrawPoint(tps_WIN_renderer, xc - x, yc + y);
    SDL_RenderDrawPoint(tps_WIN_renderer, xc + x, yc + y);
    SDL_RenderDrawPoint(tps_WIN_renderer, xc - x, yc - y);
    if (p < 0) {
      x = x + 1;
      p = p + (2 * ry * ry * x) + (ry * ry);
    } else {
      x = x + 1;
      y = y - 1;
      p = p + (2 * ry * ry * x + ry * ry) - (2 * rx * rx * y);
    }
  }
  p = ((float) x + 0.5) * ((float) x + 0.5) * ry * ry + (y - 1) * (y - 1) * rx * rx - rx * rx * ry * ry;
  while (y >= 0) {
    SDL_RenderDrawPoint(tps_WIN_renderer, xc + x, yc - y);
    SDL_RenderDrawPoint(tps_WIN_renderer, xc - x, yc + y);
    SDL_RenderDrawPoint(tps_WIN_renderer, xc + x, yc + y);
    SDL_RenderDrawPoint(tps_WIN_renderer, xc - x, yc - y);
    if (p > 0) {
      y = y - 1;
      p = p - (2 * rx * rx * y) + (rx * rx);
    } else {
      y = y - 1;
      x = x + 1;
      p = p + (2 * ry * ry * x) - (2 * rx * rx * y) - (rx * rx);
    }
  }
}
void tps_fillEllipse(int xc, int yc, int rx, int ry)
{
  int x, y, p;
  x = 0;
  y = ry;
  p = (ry * ry) - (rx * rx * ry) + ((rx * rx) / 4);
  // for(int xf = xc - rx; xf <= xc + rx; xf++) {
  //   SDL_RenderDrawPoint(tps_WIN_renderer, xf, yc);
  // }
  for(int yf = yc - ry; yf <= yc + ry; yf++) {
    SDL_RenderDrawPoint(tps_WIN_renderer, xc, yf);
  }
  while ((2 * x * ry * ry) < (2 * y * rx * rx)) {
    for(int xf = x; xf > 0; xf--) {
      SDL_RenderDrawPoint(tps_WIN_renderer, xc + xf, yc - y);
      SDL_RenderDrawPoint(tps_WIN_renderer, xc - xf, yc + y);
      SDL_RenderDrawPoint(tps_WIN_renderer, xc + xf, yc + y);
      SDL_RenderDrawPoint(tps_WIN_renderer, xc - xf, yc - y);
    }
    if (p < 0) {
      x = x + 1;
      p = p + (2 * ry * ry * x) + (ry * ry);
    } else {
      x = x + 1;
      y = y - 1;
      p = p + (2 * ry * ry * x + ry * ry) - (2 * rx * rx * y);
    }
  }
  p = ((float) x + 0.5) * ((float) x + 0.5) * ry * ry + (y - 1) * (y - 1) * rx * rx - rx * rx * ry * ry;
  while (y >= 0) {
    for(int xf = x; xf > 0; xf--) {
      SDL_RenderDrawPoint(tps_WIN_renderer, xc + xf, yc - y);
      SDL_RenderDrawPoint(tps_WIN_renderer, xc - xf, yc + y);
      SDL_RenderDrawPoint(tps_WIN_renderer, xc + xf, yc + y);
      SDL_RenderDrawPoint(tps_WIN_renderer, xc - xf, yc - y);
    }
    if (p > 0) {
      y = y - 1;
      p = p - (2 * rx * rx * y) + (rx * rx);
    } else {
      y = y - 1;
      x = x + 1;
      p = p + (2 * ry * ry * x) - (2 * rx * rx * y) - (rx * rx);
    }
  }
}


void tps_drawText(int x, int y, char* t, int fontSize) {
  _fontRW->seek(_fontRW, RW_SEEK_SET, 0);
  TTF_Font* font = TTF_OpenFontRW(_fontRW, 0, fontSize); //this opens a font style and sets a size

  SDL_Color textColor;
  SDL_GetRenderDrawColor(tps_WIN_renderer, &textColor.r, &textColor.g, &textColor.b, &textColor.a);
  SDL_Surface* textSurface = TTF_RenderUTF8_Blended(font, t, textColor);
  SDL_Texture* text = SDL_CreateTextureFromSurface(tps_WIN_renderer, textSurface);
  int text_width = textSurface->w;
  int text_height = textSurface->h;
  SDL_FreeSurface(textSurface);
  SDL_Rect renderQuad = { x, y, text_width, text_height };
  SDL_RenderCopy(tps_WIN_renderer, text, NULL, &renderQuad);
  SDL_DestroyTexture(text);

}

double tps_getFPS(void) {
  return 1000.0 / tps_deltaTime;
}

void tps_drawFPS(void) {
  char t[30] = "";
  sprintf(t, "%ffps", tps_getFPS());
  tps_drawText(0,0,t,14);
}


void tps_wait(unsigned int milliseconds) {
  SDL_Delay(milliseconds);
}


tps_texture_t *tps_createTexture(long width, long height)
{
  tps_texture_t * texture = malloc(sizeof(tps_texture_t));
  texture->width = width;
  texture->height = height;
  texture->sdl_texture = SDL_CreateTexture(tps_getRenderer(),
  SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, width, height);

  Uint32 maxIndex = width * height;

  texture->pixels = malloc(sizeof(Uint32)* maxIndex);
  memset(texture->pixels, 255, maxIndex * sizeof(Uint32));

  SDL_UpdateTexture(texture->sdl_texture, NULL, texture->pixels, sizeof(Uint32) * width);

  return texture;
}

void tps_drawTexture(tps_texture_t *texture)
{
  SDL_UpdateTexture(texture->sdl_texture, NULL, texture->pixels, texture->width * sizeof(Uint32));
  SDL_RenderCopy(tps_getRenderer(), texture->sdl_texture, NULL, NULL);
}

void tps_freeTexture(tps_texture_t *texture)
{
  if(texture)
  {
    free(texture->pixels);
    texture->pixels = NULL;

    free(texture);
  }
}

#endif //NO_UI
